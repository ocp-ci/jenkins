# OCP-CI Jenkins

## Oct 21
-  for jenkins
   - open some domains
     - *.quay.io
     - registry.redhat
     - *.docker.io
- for jenkins gitea plugin
  - open some domains
    - some university?? dont remember which

## Nov 23 plan
- git access needed on jump server
  - https://cedp-git.omega.dce-eir.net/CEDP-OCP/* preferably
  - https://cedp-git.omega.dce-eir.net/CEDP-OCP/Jenkins
  - https://cedp-git.omega.dce-eir.net/CEDP-OCP/Gitea
  - https://cedp-git.omega.dce-eir.net/CEDP-OCP/Sonarqube
  - https://cedp-git.omega.dce-eir.net/CEDP-OCP/java-hello-world-with-maven
- jenkins to use the git urls
  - https://cedp-git.omega.dce-eir.net
## Nov 23 findings
- the problems seams to be an ssl cert validation, ml to fup

## Initialisation
```
git clone --depth 1 https://gitlab.com/ocp-ci/jenkins
git config --global user.email "you@example.com"
git config --global user.name "mike-lam"
cd jenkins
git add * && git commit -m . && git push
oc new-project a1
./jenkins.sh
```


### 1st deployment
- only the openshift supplied image works
  - to customize use info at https://docs.openshift.com/container-platform/4.1/openshift_images/using_images/images-other-jenkins.html
- the script will use code as infrastructure principle and copy ./jenkins in the container
- the container will restart
- because of code as infrastructure credentials gets corupted so enter them MANUALLY
- on startup when getting Your connection is not private, use advance and then Proceed to jenkins-*.environments.katacoda.com (unsafe)
- Authentication
  - using OATH=true (default), 
    - authentication is done by openshift
    - u=admin p=admin
    - allow selected permissions
  - using OATH=false
    - authentication is done by jenkins 
    - u=admin p=password  
-oc expose jenkins-persistent not required because the template does it! 

### some configuration
need to approve scripts to use groovy scripts for cedp ocp deployment (i.e. https://cedp-git.omega.dce-eir.net/CEDP-OCP/TemplateToDeployCedpFromJenkins ). The aproval can be done in Jenkins on this page
https://jenkins-cedp-wip.apps.mpr7sikn.canadacentral.aroapp.io/scriptApproval/

### sanity check
- add item admin1 which will scan admin1 and build jenkins_pipeline_hello and java-hello-world-with-maven



